package com.binapp.escrapper.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by victor on 31/1/17.
 */

public class GoogleSearchItem {
    private String title;
    private String link;
    private String diary;
    private String date;

    public GoogleSearchItem(String title, String link, String diary, String date) {
        this.title = title;
        this.link = link;
        this.diary = diary;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDiary() {
        return diary;
    }

    public void setDiary(String diary) {
        this.diary = diary;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GoogleSearchItem that = (GoogleSearchItem) o;

        return link.equals(that.link);

    }

    @Override
    public int hashCode() {
        return link.hashCode();
    }
}
