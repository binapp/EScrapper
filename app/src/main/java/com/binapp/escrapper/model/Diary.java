package com.binapp.escrapper.model;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by victor on 17/2/17.
 */

public class Diary {
    public String urlRoot;
    public String commentsButtonXDom;
    public String commentsButtonNextDom;
    public String commentContainerXPath;
    public String articleAuthorXpath;
    public String articleNumberXpath;
    public String articleCommentXpath;
    public String articleDateXpath;


    public Diary(String urlRoot, String commentsButtonXDom, String commentsButtonNextDom, String commentContainerXPath, String articleAuthorXpath, String articleNumberXpath, String articleCommentXpath, String articleDateXpath) {
        this.urlRoot = urlRoot;
        this.commentsButtonXDom = commentsButtonXDom;
        this.commentsButtonNextDom = commentsButtonNextDom;
        this.commentContainerXPath = commentContainerXPath;
        this.articleAuthorXpath = articleAuthorXpath;
        this.articleNumberXpath = articleNumberXpath;
        this.articleCommentXpath = articleCommentXpath;
        this.articleDateXpath = articleDateXpath;
    }

    /**
     * This method must be Overriden if the Diary has his own api to get the commentaries
     * @return
     */
    public ArrayList<Commentary> getCommentsFromAPI(){
        return null;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        String thisObj = gson.toJson(this);
        return thisObj;
    }
}
