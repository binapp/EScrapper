package com.binapp.escrapper.model;
import com.google.gson.Gson;

import java.util.TreeSet;

/**
 * Created by victor on 17/2/17.
 */

public class Article {
    private String title;
    private String link;
    private String date;
    private Diary diary;
    private TreeSet<Commentary> commentaries;

    public Article(GoogleSearchItem googleSearchItem, Diary diary) {
        this.title = googleSearchItem.getTitle();
        this.link = googleSearchItem.getLink();
        this.date = googleSearchItem.getDate();
        this.diary = diary;
        this.commentaries = new TreeSet<Commentary>();
    }

    public Article(String link){
        this.link = link;
    }

    public void addMetaTags(String title, String date, Diary diary){
        this.title = title;
        this.date = date;
        this.diary = diary;

    }

    public void addCommentary(Commentary commentary){
        if (this.commentaries != null) this.commentaries.add(commentary);
        else {
            this.commentaries = new TreeSet<>();
            this.commentaries.add(commentary);
        }
    }

    public void clearCommentaries(){
        this.commentaries = new TreeSet<>();
    }

    public boolean hasNotMetaTags(){
        if (title == null && date == null && diary == null) return true;
        return false;
    }

    public TreeSet<Commentary> getCommentaries() {
        return commentaries;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getDate() {
        return date;
    }

    public Diary getDiary() {
        return diary;
    }

    public void setDiary(Diary diary) {
        this.diary = diary;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        String thisObj = gson.toJson(this);
        return thisObj;
    }
}
