package com.binapp.escrapper.model;

import com.google.gson.Gson;

/**
 * Created by victor on 17/2/17.
 */

public class Commentary implements Comparable<Commentary>{
    String number;
    String commentary;
    String author;
    String date;

    public Commentary(String number, String commentary, String author, String date) {
        this.number = number;
        this.commentary = commentary;
        this.author = author;
        this.date = date;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCommentary() {
        return this.commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Commentary)) return false;

        Commentary that = (Commentary) o;

        if (getNumber() != null ? !getNumber().equals(that.getNumber()) : that.getNumber() != null)
            return false;
        if (getCommentary() != null ? !getCommentary().equals(that.getCommentary()) : that.getCommentary() != null)
            return false;
        if (getAuthor() != null ? !getAuthor().equals(that.getAuthor()) : that.getAuthor() != null)
            return false;
        return getDate() != null ? getDate().equals(that.getDate()) : that.getDate() == null;

    }

    @Override
    public int hashCode() {
        int result = getNumber() != null ? getNumber().hashCode() : 0;
        result = 31 * result + (getCommentary() != null ? getCommentary().hashCode() : 0);
        result = 31 * result + (getAuthor() != null ? getAuthor().hashCode() : 0);
        result = 31 * result + (getDate() != null ? getDate().hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Commentary o) {

        if (o.getNumber() == null) return 1;
        if (Integer.parseInt(o.getNumber()) == Integer.parseInt(this.getNumber()))return 0;
        else if (Integer.parseInt(o.getNumber()) < Integer.parseInt(this.getNumber()))return 1;
        else return -1;
    }


    @Override
    public String toString() {
        Gson gson = new Gson();
        String thisObj = gson.toJson(this);
        return thisObj;
    }

}
