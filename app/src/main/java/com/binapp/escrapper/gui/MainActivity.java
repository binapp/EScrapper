package com.binapp.escrapper.gui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.binapp.escrapper.R;
import com.binapp.escrapper.gui.fragments.PreviousEscrapsFragment;
import com.binapp.escrapper.gui.fragments.PreviousSearchesFragment;
import com.binapp.escrapper.gui.fragments.ResultFragment;
import com.binapp.escrapper.gui.fragments.SearchFragment;
import com.binapp.escrapper.model.Article;
import com.binapp.escrapper.model.GoogleSearchItem;
import com.binapp.escrapper.model.Search;
import com.binapp.escrapper.util.GoogleCustomSearchSender;
import com.binapp.escrapper.util.JsonObject;
import com.binapp.escrapper.util.StorageController;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public ArrayList<GoogleSearchItem> foundedItems;
    FrameLayout srch_frame;
    FrameLayout tab_frame;

    SearchFragment srchF;
    String queryToSearch;
    TextView queryTV;
    SearchView searchView;
    MenuItem searchItem;
    ActionBar actionBar;

    public static int RESULT = 10;

    public boolean restartMain = true;

    private ArrayList<Article> articlesArray;
    private ArrayList<Search> searchesArray;
    private ArrayList<String> wordsArray;

    /********************
     * OVERRIDE METHODS
     ********************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Array Build
        searchesArray = new ArrayList<>();
        wordsArray = new ArrayList<>();

        setContentFromDatabase();

        // Set no title
        getSupportActionBar().setTitle("");
        srch_frame = (FrameLayout) findViewById(R.id.search_frame_content);
        tab_frame = (FrameLayout) findViewById(R.id.tabs_frame_content);

        //Init floating button
        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_button);
        final Activity activity = this;
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                // Get the layout inflater
                LayoutInflater inflater = activity.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.add_url_alert, null);
                builder.setView(dialogView);

                final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);
                builder.setTitle("Añade URL");

                builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Article article = new Article(edt.getText().toString());
                        articlesArray = new ArrayList<Article>();
                        articlesArray.add(article);


                        Intent intent = new Intent(activity,ScrappActivity.class);
                        intent.putExtra("articles", JsonObject.getStringFromArticlesArray(articlesArray));
                        startActivityForResult(intent,RESULT);

                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });

                AlertDialog b = builder.create();
                b.show();
            }
        });


        //Set tab fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.tabs_frame_content,  new PreviousEscrapsFragment()
        );
        fragmentTransaction.commit();


        srch_frame.setVisibility(View.INVISIBLE);
        launchSearchFragment();

        //Get action bar
        actionBar = getSupportActionBar();

        // Specify that tabs should be displayed in the action bar.
        actionBar.setNavigationMode(android.support.v7.app.ActionBar.NAVIGATION_MODE_TABS);

        // Create a tab listener that is called when the user changes tabs.
        android.support.v7.app.ActionBar.TabListener tabListener = new android.support.v7.app.ActionBar.TabListener() {
            @Override
            public void onTabSelected(android.support.v7.app.ActionBar.Tab tab, FragmentTransaction ft) {
                Log.e("POS", String.valueOf(tab.getPosition()));
                tab.getIcon().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment fra = null;
                if (tab.getPosition() == 0){
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.tabs_frame_content);
                    if (fragment != null && fragment.getClass().equals(PreviousSearchesFragment.class)){
                        fragmentTransaction.setCustomAnimations( R.anim.slide_in_left, R.anim.slide_out_right,0,0);
                        fragmentTransaction.replace(R.id.tabs_frame_content, new PreviousEscrapsFragment());
                        fragmentTransaction.commit();
                    }

                }else{
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.tabs_frame_content);
                    if (fragment != null && fragment.getClass().equals(PreviousEscrapsFragment.class)){
                        fragmentTransaction.setCustomAnimations( R.anim.slide_in_right, R.anim.slide_out_left,0,0);
                        fragmentTransaction.replace(R.id.tabs_frame_content, new PreviousSearchesFragment());
                        fragmentTransaction.commit();
                    }
                }
            }

            @Override
            public void onTabUnselected(android.support.v7.app.ActionBar.Tab tab, FragmentTransaction ft) {
                tab.getIcon().setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_IN);

            }

            @Override
            public void onTabReselected(android.support.v7.app.ActionBar.Tab tab, FragmentTransaction ft) {

            }
        };

        // Add 2 tabs, specifying the tab's number and TabListener
        ActionBar.Tab tab1 = actionBar.newTab();
        actionBar.addTab(tab1.setTabListener(tabListener).setIcon(R.drawable.ic_library_books_black_24dp));
        ActionBar.Tab tab2 = actionBar.newTab();
        actionBar.addTab(tab2.setTabListener(tabListener).setIcon(R.drawable.ic_star_black_24dp));
        tab2.getIcon().setColorFilter(getResources().getColor(R.color.darkGray), PorterDuff.Mode.SRC_IN);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();


        setContentFromDatabase();
        refresTabListsFragments();


        if (restartMain){
            Fragment frag = getSupportFragmentManager().findFragmentById(R.id.search_frame_content);
            if (frag != null){
                getSupportFragmentManager().beginTransaction().setCustomAnimations( 0, R.anim.fade_out,R.anim.fade_out,R.anim.fade_out).replace(R.id.search_frame_content, srchF).commit();
                srch_frame.setVisibility(View.INVISIBLE);
                actionBar.setNavigationMode(android.support.v7.app.ActionBar.NAVIGATION_MODE_TABS);
                restartMain = true;
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == RESULT) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
               restartMain = false;
            }else{
                restartMain = true;
            }
        }
    }

    @Override
    public void onBackPressed() {
        refresTabListsFragments();

        if(srch_frame.getVisibility() == View.VISIBLE){
            //Search from word
            getSupportFragmentManager().beginTransaction().setCustomAnimations( 0, R.anim.slide_out_up,0,0).replace(R.id.search_frame_content, srchF).commit();
            srch_frame.setVisibility(View.INVISIBLE);
            actionBar.setNavigationMode(android.support.v7.app.ActionBar.NAVIGATION_MODE_TABS);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                return true;

            case R.id.action_search:
                // User chose the "Favorite" action, mark the current item
                // as a favorite...
                return true;

            case android.R.id.home:
                actionBar.setDisplayHomeAsUpEnabled(false);
                onBackPressed();
                return false;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    /**
     * Allows to not restart the main when the app comes back frome the device browser
     * @param hasFocus
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        restartMain=false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        queryTV =(TextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchItem = menu.findItem(R.id.action_search);
        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                if (srch_frame !=null){
                    srch_frame.setVisibility(View.INVISIBLE);
                    actionBar.setNavigationMode(android.support.v7.app.ActionBar.NAVIGATION_MODE_TABS);
                }
                return true;
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                launchSearchFragment();
                srch_frame.setVisibility(View.VISIBLE);
                // Specify that tabs should be displayed in the action bar.
                actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                new RetrieveFeedTask(srchF.getContext(), query, srchF.diariesSpinner.getSelectedItem().toString(),srchF.fromDateTextView.getText().toString(),srchF.toDateTextView.getText().toString()).execute();
                return true;
            }

        });

        return true;
    }



    /********************
     * PRIVATE METHODS
     ********************/



    /**
     * Refresh the lists Fragment inserted in the TabBar
     */

    private void refresTabListsFragments() {
        Fragment fragmentToRefresh = getSupportFragmentManager().findFragmentById(R.id.tabs_frame_content);
        if (fragmentToRefresh != null){
            if(fragmentToRefresh.getClass().equals(PreviousEscrapsFragment.class)){
                PreviousEscrapsFragment prev = (PreviousEscrapsFragment) fragmentToRefresh;
                prev.populateListView();
            }else if(fragmentToRefresh.getClass().equals(PreviousSearchesFragment.class)){
                PreviousSearchesFragment prev = (PreviousSearchesFragment) fragmentToRefresh;
                prev.populateListView();
            }
        }
    }

    /**
     * Update the database
     */
    private void setContentFromDatabase(){
        StorageController sc = new StorageController(getApplicationContext());
        searchesArray = sc.getSearchesFromDataBase();
        wordsArray = sc.getWordsFromDataBase();
    }

    /**
     * Launch the search fragment without pre defined word
     */
    private  void launchSearchFragment(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_down,0,0,0);
        srchF = new SearchFragment();
        fragmentTransaction.replace(R.id.search_frame_content, srchF);
        fragmentTransaction.commit();
    }


    /**
     * Launch the result fragment
     * Starts the GogleCustomSearchSender with data
     * @param diary
     * @param fromDate
     * @param toDate
     * @param query
     */
    private void launchResultFragment(String diary, String fromDate, String toDate, String query) {
        new GoogleCustomSearchSender(this,diary, fromDate.replace("/",""), toDate.replace("/",""), query);
        //Launch result
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations( R.anim.fade_in, 0,0,0);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        ResultFragment fragment = new ResultFragment();
        fragmentTransaction.replace(R.id.search_frame_content, fragment);
        fragmentTransaction.commit();
    }

    /********************
     * PUBLIC METHODS
     ********************/

    /**
     * Launch the search fragment with word
     * @param word
     */
    public void launchSearchFragment(String word) {
        srch_frame.setVisibility(View.VISIBLE);
        // Specify that tabs should be displayed in the action bar.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayHomeAsUpEnabled(true);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_down,0,0,0);
        srchF = new SearchFragment();
        srchF.setWord(word);
        fragmentTransaction.replace(R.id.search_frame_content, srchF);
        fragmentTransaction.commit();

    }

    /**
     * Fill the ResultFragment List with the ArrayList founded items
     * @param foundedItems ArrayList<GoogleSearchItem
     */
    public void addFoundedItems(ArrayList<GoogleSearchItem> foundedItems){
        this.foundedItems.addAll(foundedItems);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.search_frame_content);
        if (fragment != null && fragment.getClass().equals(ResultFragment.class)){
            ResultFragment result = (ResultFragment) fragment;
            result.populateListView(this.foundedItems);
        }
    }

    /**
     * init the Array also before onStart
     */
    public void initFoudnedItems(){
        this.foundedItems = new ArrayList<>();
    }

    /**
     * Setter
     * @param articlesArray
     */
    public void setArticlesArray(ArrayList<Article> articlesArray) {
        this.articlesArray = articlesArray;
    }

    /**
     * Check if the searched String is a valid URL before launch the engine
     * @param diary
     * @param fromDate
     * @param toDate
     */

    public void checkUrlBeforeEngineAsyncTask(String diary, String fromDate, String toDate){
        queryToSearch = queryTV.getText().toString();
        new RetrieveFeedTask(queryTV.getContext(),queryToSearch, diary, fromDate, toDate).execute();
    }

    /**
     * Check if the searched String is a valid URL before launch the engine.
     * @param word  the word is given from the database previous searches
     * @param diary
     * @param fromDate
     * @param toDate
     */
    public void checkUrlBeforeEngineAsyncTask(String word, String diary, String fromDate, String toDate){
        new RetrieveFeedTask(queryTV.getContext(),word, diary, fromDate, toDate).execute();
    }

    /********************
     * INNER CLASSES
     ********************/

    class RetrieveFeedTask extends AsyncTask<String, Void, Boolean> {

        private String url;
        private String diary;
        private String dateFrom;
        private String dateTo;
        private Context context;

        public RetrieveFeedTask(Context context, String url, String diary, String dateFrom, String dateTo) {
            this.url = url;
            this.diary = diary;
            this.dateFrom =  dateFrom;
            this.dateTo = dateTo;
            this.context = context;

        }

        protected Boolean doInBackground(String... urls) {

            if (!Patterns.WEB_URL.matcher(this.url).matches()) return new Boolean(false);

            URL url = null;
            try {

                url = new URL(this.url);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                int code = connection.getResponseCode();
                if(code == 200) {
                    return new Boolean(true);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return new Boolean(false);

        }

        protected void onPostExecute(Boolean result) {
            if(result){
                //launchScrapFragment();
                Toast.makeText(getApplicationContext(), "This is a valid url to scrap", Toast.LENGTH_SHORT).show();
            } else {
                searchItem.collapseActionView();
                srch_frame.setVisibility(View.VISIBLE);
                queryToSearch = queryTV.getText().toString();

                //Add to database
                StorageController sc = new StorageController(context);
                sc.addWordToDataBase(url);

                launchResultFragment(diary,dateFrom,dateTo,url);
            }
        }
    }
}
