package com.binapp.escrapper.gui.swipe_adapter;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.binapp.escrapper.gui.swipe_adapter.RecyclerSearchAdapterViewAdapter;

/**
 * Created by victor on 7/2/17.
 */

public class SearchSimpleItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private final RecyclerSearchAdapterViewAdapter searchAdapter;

    public SearchSimpleItemTouchHelperCallback(RecyclerSearchAdapterViewAdapter adapter) {
        searchAdapter = adapter;
    }


    @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return true;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        //int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        int dragFlags = 0;
        int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                          RecyclerView.ViewHolder target) {
        searchAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
       searchAdapter.onItemDismiss(viewHolder.getAdapterPosition());
    }
}
