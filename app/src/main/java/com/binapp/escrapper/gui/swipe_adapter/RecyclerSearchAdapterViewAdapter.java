package com.binapp.escrapper.gui.swipe_adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.binapp.escrapper.R;
import com.binapp.escrapper.gui.MainActivity;
import com.binapp.escrapper.gui.fragments.PreviousEscrapsFragment;
import com.binapp.escrapper.model.Search;
import com.binapp.escrapper.util.StorageController;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by victor on 22/2/17.
 */

public class RecyclerSearchAdapterViewAdapter extends RecyclerView.Adapter<RecyclerSearchAdapterViewAdapter.SearchItemViewHolder> implements ItemTouchHelperAdapter {
    private final ArrayList<Search> values;
    private final Context context;
    private final OnStartDragListener mDragStartListener;

    public ArrayList<Search> getValues() {
        return values;
    }

    public RecyclerSearchAdapterViewAdapter(Context context, ArrayList<Search> values, OnStartDragListener dragStartListener) {
        mDragStartListener = dragStartListener;
        this.context = context;
        this.values = values;
    }

    @Override
    public RecyclerSearchAdapterViewAdapter.SearchItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_search_list_view,parent,false);
        RecyclerSearchAdapterViewAdapter.SearchItemViewHolder holder = new RecyclerSearchAdapterViewAdapter.SearchItemViewHolder(v);
        return holder;
    }


    @Override
    public void onBindViewHolder(final RecyclerSearchAdapterViewAdapter.SearchItemViewHolder holder, final int position) {
        Search searchItem = values.get(position);
        holder.number.setText(""+searchItem.getArticles().size());
        holder.date.setText(searchItem.getDate());
        //TO DO
        holder.diary.setText("");
        holder.word.setText("");


        // Start a drag whenever the handle view it touched
        /*
        holder.foregroundView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Search searchItem = values.get(position);

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchItem.getLink()));
                    context.startActivity(intent);

                }
                return true;
            }
        });
        */

    }

    public void remove(int position){

        Search search = values.get(position);

        //remove from list
        values.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount());

        //RemoveFromDatabase to database
        StorageController sc = new StorageController(context);
        sc.deleteSearchToDataBase(search);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    @Override
    public void onItemDismiss(int position) {
        remove(position);
        //values.remove(position);
        //notifyItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        /*
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(values, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(values, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        */
        return true;
    }


    static class SearchItemViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder{
        protected TextView number;
        protected TextView diary;
        protected TextView date;
        protected TextView word;

        protected RelativeLayout foregroundView;


        public SearchItemViewHolder(View itemView) {
            super(itemView);
            number = (TextView) itemView.findViewById(R.id.number);
            diary = (TextView) itemView.findViewById(R.id.diary);
            date = (TextView) itemView.findViewById(R.id.date);
            word = (TextView) itemView.findViewById(R.id.word);
            foregroundView = (RelativeLayout) itemView.findViewById(R.id.foreground);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }


}
