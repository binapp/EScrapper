package com.binapp.escrapper.gui.swipe_adapter;

/**
 * Created by victor on 31/1/17.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.binapp.escrapper.R;
import com.binapp.escrapper.gui.MainActivity;
import com.binapp.escrapper.model.GoogleSearchItem;
import com.binapp.escrapper.model.Search;
import com.binapp.escrapper.util.StorageController;
import com.google.gson.Gson;

import java.util.ArrayList;

public class ReclyclerGoogleSearchItemViewArrayAdapter extends RecyclerView.Adapter<ReclyclerGoogleSearchItemViewArrayAdapter.GoogleSearchItemViewHolder> implements ItemTouchHelperAdapter {
    private final ArrayList<GoogleSearchItem> values;
    private final Context context;
    private final OnStartDragListener mDragStartListener;

    public ArrayList<GoogleSearchItem> getValues() {
        return values;
    }

    public ReclyclerGoogleSearchItemViewArrayAdapter(Context context, ArrayList<GoogleSearchItem> values, OnStartDragListener dragStartListener) {
        mDragStartListener = dragStartListener;
        this.context = context;
        this.values = values;
    }

    @Override
    public GoogleSearchItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_result_google_search_item,parent,false);
        GoogleSearchItemViewHolder holder = new GoogleSearchItemViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final GoogleSearchItemViewHolder holder, final int position) {
        GoogleSearchItem googleSearchItem = values.get(position);
        holder.text.setText(googleSearchItem.getTitle());
        holder.text3.setText(googleSearchItem.getDate());
        Drawable image = null;
        if(values.get(position).getDiary().contains("elpais.com")){
            image = ContextCompat.getDrawable(context,R.drawable.elpais);
        }else if(googleSearchItem.getDiary().contains("20minutos.es")||googleSearchItem.getDiary().contains("20minutos.com")){
            image = ContextCompat.getDrawable(context,R.drawable.minutos);
        }else if(googleSearchItem.getDiary().contains("abc.es")){
            image = ContextCompat.getDrawable(context,R.drawable.abc);
        }else if(googleSearchItem.getDiary().contains("elmundo.es")){
            image = ContextCompat.getDrawable(context,R.drawable.elmundo);
        }else if(googleSearchItem.getDiary().contains("eldiario.es")){
            image = ContextCompat.getDrawable(context,R.drawable.diariodigital);
        }else if(googleSearchItem.getDiary().contains("elconfidencial.com")){
            image = ContextCompat.getDrawable(context,R.drawable.confidencial);
        }
        holder.icon.setImageDrawable(image);

        // Start a drag whenever the handle view it touched
        holder.foregroundView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    GoogleSearchItem searchItem = values.get(position);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(searchItem.getLink()));
                    context.startActivity(intent);
                }
                return true;
            }
        });

    }

    public void remove(int position){
        values.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount());

    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    @Override
    public void onItemDismiss(int position) {
        remove(position);
        //values.remove(position);
        //notifyItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        /*
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(values, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(values, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        */
        return true;
    }


    static class GoogleSearchItemViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder{
        protected TextView text;
        protected TextView text3;
        protected ImageView icon;

        protected RelativeLayout foregroundView;


        public GoogleSearchItemViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.firstLine);
            text3 = (TextView) itemView.findViewById(R.id.thirdLine);
            icon = (ImageView) itemView.findViewById(R.id.icon);
            foregroundView = (RelativeLayout) itemView.findViewById(R.id.foreground);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

}