package com.binapp.escrapper.gui.swipe_adapter;

/**
 * Created by victor on 7/2/17.
 */

public interface ItemTouchHelperAdapter {
    boolean onItemMove(int fromPosition, int toPosition);
    void onItemDismiss(int position);
}
