package com.binapp.escrapper.gui.swipe_adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.binapp.escrapper.R;
import com.binapp.escrapper.gui.MainActivity;
import com.binapp.escrapper.model.Search;
import com.binapp.escrapper.util.StorageController;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * Created by victor on 22/2/17.
 */

public class RecyclerStringWordAdapterViewAdapter extends RecyclerView.Adapter<RecyclerStringWordAdapterViewAdapter.SearchItemViewHolder> implements ItemTouchHelperAdapter {
    private final ArrayList<String> values;
    private final Context context;
    private final OnStartDragListener mDragStartListener;

    public ArrayList<String> getValues() {
        return values;
    }

    public RecyclerStringWordAdapterViewAdapter(Context context, ArrayList<String> values, OnStartDragListener dragStartListener) {
        mDragStartListener = dragStartListener;
        this.context = context;
        this.values = values;
    }

    @Override
    public RecyclerStringWordAdapterViewAdapter.SearchItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_word_list_view,parent,false);
        RecyclerStringWordAdapterViewAdapter.SearchItemViewHolder holder = new RecyclerStringWordAdapterViewAdapter.SearchItemViewHolder(v);
        return holder;
    }


    @Override
    public void onBindViewHolder(final RecyclerStringWordAdapterViewAdapter.SearchItemViewHolder holder, final int position) {
        String word = values.get(position);
        holder.word.setText(word);


        // Start a drag whenever the handle view it touched

        holder.foregroundView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    String word = values.get(position);
                    MainActivity mainActivity = (MainActivity) context;
                    mainActivity.launchSearchFragment(word);
                }
                return true;
            }
        });


    }

    public void remove(int position){

        if (position<values.size()){
            String word = values.get(position);

            //RemoveFromDatabase to database
            StorageController sc = new StorageController(context);
            sc.deleteWordToDataBase(word);

            //remove from list
            values.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, getItemCount());
        }
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    @Override
    public void onItemDismiss(int position) {
        remove(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        /*
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(values, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(values, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        */
        return true;
    }


    static class SearchItemViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder{
        protected TextView word;
        protected RelativeLayout foregroundView;


        public SearchItemViewHolder(View itemView) {
            super(itemView);
            word = (TextView) itemView.findViewById(R.id.word);
            foregroundView = (RelativeLayout) itemView.findViewById(R.id.list_frame_layout);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }

    }


}
