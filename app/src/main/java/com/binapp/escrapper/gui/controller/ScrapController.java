package com.binapp.escrapper.gui.controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;


import com.binapp.escrapper.gui.MainActivity;
import com.binapp.escrapper.gui.ScrappActivity;
import com.binapp.escrapper.model.Article;
import com.binapp.escrapper.model.Commentary;
import com.binapp.escrapper.model.Diary;
import com.binapp.escrapper.model.Search;
import com.binapp.escrapper.util.StorageController;
import com.google.gson.Gson;

import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.List;


import us.codecraft.xsoup.Xsoup;

import static android.content.ContentValues.TAG;

/**
 * Created by victor on 16/2/17.
 */

public class ScrapController {
    private Context context;
    private WebView webView;
    private ArrayList<Article> articles;
    //test
    int position;

    private boolean isRedirected;


    //Init the webView

    public ScrapController(Context context, final WebView webView) {
        this.context = context;
        this.webView = webView;
        this.position = 0;



        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setAllowContentAccess(true);
        settings.setBlockNetworkImage(true);
        settings.setBlockNetworkLoads(false);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setSupportMultipleWindows(true);
        settings.setLoadsImagesAutomatically(false);
        settings.setUserAgentString("Mozilla/5.0 (Linux; Android 4.4.4; One Build/KTU84L.H4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.135 Mobile Safari/537.36");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            settings.setMediaPlaybackRequiresUserGesture(true);
        }


        webView.setWebChromeClient(new WebChromeClient());

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view,url,favicon);
                Log.e(TAG, "onPageStarted  "+url);
                if (!isRedirected) {
                    //Do something you want when starts loading
                }

                isRedirected = false;

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.e(TAG, "shouldOverrideUrlLoading ");
                view.loadUrl("www.google.com");
                isRedirected = true;
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                    if (!isRedirected && position<articles.size()){
                        fillArticle();
                        Article article = articles.get(position);
                        webView.loadUrl("javascript:window.onload = function() {if("+article.getDiary().commentsButtonXDom+"){"+article.getDiary().commentsButtonXDom+".click(); Android.pressButton();}else{Android.addPosition()}}");

                        //TEST
                        //webView.loadUrl("javascript: Android.analyzeText(document.documentElement.outerHTML)");
                        //http://stackoverflow.com/questions/16709963/programmatic-click-in-android-webview
                    }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e(TAG, "Error: " + description);
            }
        });

        webView.addJavascriptInterface(new EscrapperJSInterface(this.context),"Android");
    }

    private void fillArticle() {

        if (articles.get(position).hasNotMetaTags()){
            Diary diaryElMundo = DiaryController.getDiaryFromUrl(articles.get(position).getLink());
            articles.get(position).setDiary(diaryElMundo);
        }
    }


    //The return from web

    public class EscrapperJSInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        EscrapperJSInterface(Context c) {
            mContext = c;
        }


        /** Show a toast from the web page */
        @JavascriptInterface
        public void pressButton() {
            webView.post(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "pressButton ");

                    Article article = articles.get(position);
                    webView.loadUrl("javascript:if("+article.getDiary().commentsButtonNextDom+"){"+article.getDiary().commentsButtonNextDom+".click(); Android.captureComments(document.documentElement.outerHTML,0);}else{Android.captureComments(document.documentElement.outerHTML,1);}");

                }
            });
        }

        @JavascriptInterface
        public void captureComments(String html, int isFinal){
            sleep(500);

            Document doc = Jsoup.parse(html);
            Article article = articles.get(position);
            //Log.e(TAG, article.getDiary().commentContainerXPath);
            //Add article if not data
            if (article.hasNotMetaTags()){
                String title = doc.select("title").first().attr("content");
                String keywords = doc.select("meta[name=keywords]").first().attr("content");
                String description = doc.select("meta[name=description]").get(0).attr("content");
                article.setTitle(title);
                article.setDate(description);
            }


            List<String> articleDocList = Xsoup.compile(article.getDiary().commentContainerXPath).evaluate(doc).list();
            for(int i = 0 ; i < articleDocList.size(); i++){
                String articleHTML = articleDocList.get(i);
                //Log.e(TAG,articleHTML);
                Document articleDoc = Jsoup.parse(articleHTML);
                String author =  Xsoup.compile(article.getDiary().articleAuthorXpath).evaluate(articleDoc).get();
                author = Jsoup.parse(author).text();
                String number =  Xsoup.compile(article.getDiary().articleNumberXpath).evaluate(articleDoc).get();
                number = Jsoup.parse(number).text();
                String comment =  Xsoup.compile(article.getDiary().articleCommentXpath).evaluate(articleDoc).get();
                comment = Jsoup.parse(comment).text();
                String date =  Xsoup.compile(article.getDiary().articleDateXpath).evaluate(articleDoc).get();
                date = Jsoup.parse(date).text();
                Commentary commentary = new Commentary(number,comment,author,date);
                //Toast.makeText(context, commentary.toString(), Toast.LENGTH_SHORT).show();
                article.addCommentary(commentary);
            }

            if (isFinal == 0){
                pressButton();
            }else{
                addPosition();
            }


        }

        @JavascriptInterface
        public void stop(){
            Log.e(TAG,"finalize");
            webView.post(new Runnable() {
                @Override
                public void run() {
                    webView.setVisibility(View.INVISIBLE);
                    webView.stopLoading();


                    Search search = new Search(articles);
                    //MainActivity main = MainActivity.getInstance();
                    //main.addSearch(search);

                    ScrappActivity scrapA = (ScrappActivity) context;
                    scrapA.backCancell();

                    //Add to database
                    StorageController sc = new StorageController(context);
                    sc.addSeachToDataBase(search);
                }
            });
        }

        @JavascriptInterface
        public void toastTheComments(String htmlComments){
            //Toast.makeText(context,htmlComments,Toast.LENGTH_SHORT).show();

        }
        @JavascriptInterface
        public void addPosition(){


            position++;
                webView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (position<articles.size()) {
                            Log.e(TAG, "addPosition " + position);
                            webView.loadUrl(articles.get(position).getLink());
                        }else {
                            webView.loadUrl("javascript:Android.stop();");
                        }
                    }
                });


        }

        @JavascriptInterface
        public void sleep(int millis){
            try {
                Thread.sleep(millis);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void analyzeText(String html){
            Document doc = Jsoup.parse(html);
            Log.e(TAG,html);
        }
    }

    public final int getPosition(){
        return this.position;
    }

    public void initWithArticles(ArrayList<Article> articles){
        this.articles = articles;
        webView.loadUrl(articles.get(position).getLink());
    }


}
