package com.binapp.escrapper.gui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;


import com.binapp.escrapper.R;
import com.binapp.escrapper.gui.MainActivity;
import com.binapp.escrapper.gui.ScrappActivity;
import com.binapp.escrapper.gui.controller.DiaryController;
import com.binapp.escrapper.gui.swipe_adapter.OnStartDragListener;
import com.binapp.escrapper.gui.swipe_adapter.ReclyclerGoogleSearchItemViewArrayAdapter;
import com.binapp.escrapper.model.Article;
import com.binapp.escrapper.model.GoogleSearchItem;
import com.binapp.escrapper.gui.swipe_adapter.GoogleItemResultSimpleItemTouchHelperCallback;
import com.binapp.escrapper.util.JsonObject;

import java.util.ArrayList;

/**
 * Created by victor on 30/1/17.
 */

public class ResultFragment extends Fragment implements OnStartDragListener, View.OnTouchListener{
    RecyclerView recyclerView;
    ProgressBar progressBar;
    ItemTouchHelper touchHelper;
    Button scrappButton;
    ReclyclerGoogleSearchItemViewArrayAdapter adapter;
    ArrayList<GoogleSearchItem> googleSearchItemsArray;
    ArrayList<Article> articlesArray;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_result_fragment,
                container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.results_recycler_view);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        recyclerView.addItemDecoration(new DividerItemDecoration(this.getActivity(), LinearLayout.VERTICAL));


        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        googleSearchItemsArray = new ArrayList<>();

        scrappButton = (Button) view.findViewById(R.id.scrapp_btn);
        scrappButton.setOnTouchListener(this);
        return view;
    }

    public void populateListView(ArrayList<GoogleSearchItem> gsitems){
        googleSearchItemsArray.addAll(gsitems);

        adapter = new ReclyclerGoogleSearchItemViewArrayAdapter(getActivity(), googleSearchItemsArray, this);
        recyclerView.setAdapter(adapter);

        ItemTouchHelper.Callback callback =
                new GoogleItemResultSimpleItemTouchHelperCallback(adapter);
        touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);

        recyclerView.refreshDrawableState();
        progressBar.setVisibility(View.GONE);
    }


    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        touchHelper.startDrag(viewHolder);
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        //When the scrap button is done

        if (adapter != null){
            this.googleSearchItemsArray = adapter.getValues();
            articlesArray = new ArrayList<>();
            for (GoogleSearchItem gs : googleSearchItemsArray){
                Article article = new Article(gs, DiaryController.getDiaryFromUrl(gs.getLink()));
                this.articlesArray.add(article);
            }
        }

        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.setArticlesArray(articlesArray);


        if(event.getAction() == MotionEvent.ACTION_UP){
            Intent intent = new Intent(getActivity(),ScrappActivity.class);
            intent.putExtra("articles", JsonObject.getStringFromArticlesArray(articlesArray));
            getActivity().startActivityForResult(intent,MainActivity.RESULT);
            return true;
        }
        return false;
    }

    public ArrayList<Article> getArticlesArray() {
        return articlesArray;
    }
}
