package com.binapp.escrapper.gui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.WindowManager;
import android.webkit.WebView;

import com.binapp.escrapper.R;
import com.binapp.escrapper.gui.controller.ScrapController;
import com.binapp.escrapper.model.Article;
import com.binapp.escrapper.util.JsonObject;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by victor on 11/2/17.
 */

public class ScrappActivity extends AppCompatActivity {

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrapp);
        webView = (WebView) findViewById(R.id.web_view);

        // Set no title
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Mantain screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


    }

    @Override
    protected void onStart() {
        super.onStart();

        ScrapController sc = new ScrapController(this, webView);
        Gson gson = new Gson();
        Intent intent = getIntent();
        String articlesString = intent.getStringExtra("articles");
        ArrayList<Article> articles = JsonObject.getArticlesFromString(articlesString);
        //Article article = new Article("http://www.elmundo.es/economia/2017/02/13/58a1a397468aeb27778b462d.html");
        //articles.add(article);

        //Article article3 = new Article("http://www.elmundo.es/deportes/2017/02/18/58a8c68f22601d59538b45b5.html");
        //articles.add(article3);
        //Article article2 = new Article("http://www.elmundo.es/espana/2017/02/17/58a770d522601df0258b4641.html");
        //articles.add(article2);
        //Article article4 = new Article("http://www.elmundo.es/sociedad/2017/02/17/58a6ee24268e3e15028b4617.html");
        //articles.add(article4);

        sc.initWithArticles(articles);
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle("Your Title");
        // set dialog message
        alertDialogBuilder
                .setMessage("Click yes to exit!")
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, close
                        // current activity
                        backOk();
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();


    }

    public void backOk(){
        Intent data = new Intent();
        setResult(Activity.RESULT_OK,  data);
        finish();
    }

    public void backCancell(){
        Intent data = new Intent();
        setResult(Activity.RESULT_CANCELED, data);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
