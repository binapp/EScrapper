package com.binapp.escrapper.gui.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.binapp.escrapper.R;
import com.binapp.escrapper.gui.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by victor on 29/1/17.
 */

public class SearchFragment extends Fragment {

    Calendar myCalendar1 = Calendar.getInstance();
    Calendar myCalendar2 = Calendar.getInstance();
    public TextView fromDateTextView;
    public TextView toDateTextView;
    public Spinner diariesSpinner;
    public Button startSearch;
    public TextView wordToSearch;
    public String word;

    DatePickerDialog.OnDateSetListener dateFrom = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar1.set(Calendar.YEAR, year);
            myCalendar1.set(Calendar.MONTH, monthOfYear);
            myCalendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateFrom();
        }

    };

    DatePickerDialog.OnDateSetListener dateTo = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar2.set(Calendar.YEAR, year);
            myCalendar2.set(Calendar.MONTH, monthOfYear);
            myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateTo();
        }

    };





    private void updateFrom() {

        String myFormat = "yyyy/MM/dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        fromDateTextView.setText(sdf.format(myCalendar1.getTime()));
    }

    private void updateTo() {

        String myFormat = "yyyy/MM/dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        toDateTextView.setText(sdf.format(myCalendar2.getTime()));
    }

    public void dismissKeyboard(){
        InputMethodManager inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getView().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }



    @Override
    public void onStart() {
        super.onStart();

        LinearLayout fromDateSelector= (LinearLayout) getView().findViewById(R.id.date_from_selector);
        final LinearLayout toDateSelector= (LinearLayout) getView().findViewById(R.id.date_to_selector);

        fromDateTextView = (TextView) getView().findViewById(R.id.date_from_text);
        toDateTextView = (TextView) getView().findViewById(R.id.date_to_text);

        wordToSearch = (TextView) getView().findViewById(R.id.word_to_search);

        if (word != null){
            this.wordToSearch.setText(word);
            this.wordToSearch.setVisibility(View.VISIBLE);
        }else wordToSearch.setVisibility(View.GONE);

        updateFrom();
        updateTo();

        final Context context = this.getContext();
        fromDateSelector.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismissKeyboard();
                // TODO Auto-generated method stub
                new DatePickerDialog(context, dateFrom, myCalendar1
                        .get(Calendar.YEAR), myCalendar1.get(Calendar.MONTH),
                        myCalendar1.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        toDateSelector.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismissKeyboard();
                // TODO Auto-generated method stub
                new DatePickerDialog(context, dateTo, myCalendar2
                        .get(Calendar.YEAR), myCalendar2.get(Calendar.MONTH),
                        myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        diariesSpinner = (Spinner) getView().findViewById(R.id.diaries_spinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getView().getContext(),
                R.array.diaries_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        diariesSpinner.setAdapter(adapter);
        diariesSpinner.setSelection(0);
        diariesSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm=(InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                return false;
            }
        }) ;

        startSearch = (Button) getView().findViewById(R.id.search_button);
        startSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity mainActivity = (MainActivity) getActivity();
                if (wordToSearch.getVisibility() == View.VISIBLE){
                    mainActivity.checkUrlBeforeEngineAsyncTask(wordToSearch.getText().toString(), diariesSpinner.getSelectedItem().toString(),fromDateTextView.getText().toString(),toDateTextView.getText().toString());
                }else{
                    mainActivity.checkUrlBeforeEngineAsyncTask(diariesSpinner.getSelectedItem().toString(),fromDateTextView.getText().toString(),toDateTextView.getText().toString());
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_selector_frame_layout,
                container, false);
        return view;
    }

    public void setWord(String word){
        this.word = word;
    }

}
