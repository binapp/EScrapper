package com.binapp.escrapper.gui.controller;

import com.binapp.escrapper.model.Diary;

/**
 * Created by victor on 22/2/17.
 */

public class DiaryController {

    public static Diary getDiaryFromUrl(String url){
        //PROVISIONAL
        String basicUrl = "www.elmundo.es";
        String commentsUrl = "document.getElementsByClassName('icon-comentarios2_36_d')[0]";
        //String commentsUrl = "document.getElementsByClassName('icon-adelante_32_d')[0]";
        //String moreUrl = "document.getElementById('botonMas')";
        String moreUrl = "document.getElementsByClassName('icon-abajo_16_d')[0]";
        String commentsContainerXpath = "//section/article";
        String articleAuthor = "//header/div/span";
        String articleNumber =  "//span/a";
        String articleComment = "//div/p";
        String articleDate = "//header/time";
        Diary diaryElMundo = new Diary(basicUrl,commentsUrl,moreUrl,commentsContainerXpath,articleAuthor,articleNumber,articleComment,articleDate);
        return diaryElMundo;
    }

}
