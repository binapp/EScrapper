package com.binapp.escrapper.gui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.binapp.escrapper.R;
import com.binapp.escrapper.gui.MainActivity;
import com.binapp.escrapper.gui.swipe_adapter.OnStartDragListener;
import com.binapp.escrapper.gui.swipe_adapter.RecyclerSearchAdapterViewAdapter;
import com.binapp.escrapper.model.Search;
import com.binapp.escrapper.gui.swipe_adapter.SearchSimpleItemTouchHelperCallback;
import com.binapp.escrapper.util.StorageController;

import java.util.ArrayList;

/**
 * Created by victor on 12/2/17.
 */

public class PreviousEscrapsFragment extends Fragment implements OnStartDragListener, View.OnTouchListener{
    RecyclerView recyclerSearchView;
    ItemTouchHelper touchHelper;
    RecyclerSearchAdapterViewAdapter adapter;
    ArrayList<Search> searchItemsArray;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.prev_escraps_fragment,
                container, false);

        recyclerSearchView = (RecyclerView) view.findViewById(R.id.list_search);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerSearchView.setLayoutManager(llm);
        recyclerSearchView.addItemDecoration(new DividerItemDecoration(this.getActivity(), LinearLayout.VERTICAL));

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        populateListView();
    }

    public void populateListView(){
        if (recyclerSearchView != null){


            StorageController storageController = new StorageController(this.getContext());
            searchItemsArray = storageController.getSearchesFromDataBase();
            RecyclerSearchAdapterViewAdapter adapter = new RecyclerSearchAdapterViewAdapter(this.getContext(), searchItemsArray, this);
            recyclerSearchView.setAdapter(adapter);

            ItemTouchHelper.Callback callback =
                    new SearchSimpleItemTouchHelperCallback(adapter);
            touchHelper = new ItemTouchHelper(callback);
            touchHelper.attachToRecyclerView(recyclerSearchView);
            recyclerSearchView.refreshDrawableState();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        touchHelper.startDrag(viewHolder);
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        //When the scrap button is done

        if (adapter != null){
            this.searchItemsArray = adapter.getValues();
            searchItemsArray = new ArrayList<>();
            for (Search gs : searchItemsArray){
                /*
                Article article = new Article(gs, DiaryController.getDiaryFromUrl(gs.getLink()));
                this.articlesArray.add(article);
                */
            }
        }

        /*
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.setArticlesArray(articlesArray);


        if(event.getAction() == MotionEvent.ACTION_UP){
            Intent i = new Intent(getActivity(), ScrappActivity.class);
            startActivity(i);
            return true;
        }
        */
        return false;
    }

}
