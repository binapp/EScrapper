package com.binapp.escrapper.util;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

import com.binapp.escrapper.gui.MainActivity;
import com.binapp.escrapper.R;
import com.binapp.escrapper.model.GoogleSearchItem;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by victor on 30/1/17.
 */

public class GoogleCustomSearchSender {

    private final String CUSTOM_SEARCH_URL =  "https://www.googleapis.com/customsearch/v1?";
    private String API_KEY;
    private String ENGINE_ALL;
    private String ENGINE_ELPAIS;
    private String ENGINE_ELCONFIDENCIAL;
    private String ENGINE_ELDIARIOES;
    private String ENGINE_ELMUNDO;
    private String QUERY_FIELDS = "&fields=queries,items(title,link,displayLink,pagemap(metatags(date)))";
    private String QUERY_SORT_RANGE = "&sort=date:r:";
    private String QUERY;
    private String START_AT_COUNTER = "&start=";
    private ArrayList<GoogleSearchItem> items;

    public GoogleCustomSearchSender(Context context, String diaries, String dateFrom, String dateTo, String qry){
        items = new ArrayList<>();

        MainActivity mainActivity = (MainActivity) context;
        mainActivity.initFoudnedItems();


        fillTheContent(context, dateFrom, dateTo, qry);
        String urlToSearch = new String();
        if(diaries.equals("EL PAIS")){
            urlToSearch = CUSTOM_SEARCH_URL+API_KEY + ENGINE_ELPAIS + QUERY + QUERY_SORT_RANGE + QUERY_FIELDS;
            new GoogleSearchApiConnector(context,urlToSearch,1).execute();
        }else if(diaries.equals("El Confidencial")){
            urlToSearch = CUSTOM_SEARCH_URL+API_KEY + ENGINE_ELCONFIDENCIAL + QUERY + QUERY_SORT_RANGE + QUERY_FIELDS;
            new GoogleSearchApiConnector(context,urlToSearch,1).execute();
        }else if(diaries.equals("El Diario.es")){
                urlToSearch = CUSTOM_SEARCH_URL+API_KEY + ENGINE_ELDIARIOES + QUERY + QUERY_SORT_RANGE + QUERY_FIELDS;
            new GoogleSearchApiConnector(context,urlToSearch,1).execute();
        }else if(diaries.equals("El Mundo")){
            urlToSearch = CUSTOM_SEARCH_URL+API_KEY + ENGINE_ELDIARIOES + QUERY + QUERY_SORT_RANGE + QUERY_FIELDS;
            new GoogleSearchApiConnector(context,urlToSearch,1).execute();
        }else{
            urlToSearch = CUSTOM_SEARCH_URL+API_KEY + ENGINE_ALL + QUERY + QUERY_SORT_RANGE + QUERY_FIELDS;
            new GoogleSearchApiConnector(context,urlToSearch,1).execute();
        }


    }




    private void fillTheContent(Context context, String dateFrom, String dateTo, String qry) {
        Resources res = context.getResources();
        String api = res.getString(R.string.api_key);

        API_KEY = "key=" + context.getResources().getString(R.string.api_key);
        ENGINE_ALL = "&cx=" + context.getResources().getString(R.string.search_engine_all);
        ENGINE_ELPAIS = "&cx=" + context.getResources().getString(R.string.search_engine_elpais);
        ENGINE_ELCONFIDENCIAL = "&cx=" + context.getResources().getString(R.string.search_engine_elconfidencial);
        ENGINE_ELDIARIOES = "&cx=" + context.getResources().getString(R.string.search_engine_diarioes);
        ENGINE_ELDIARIOES = "&cx=" + context.getResources().getString(R.string.search_engine_elmundo);
        try {
            QUERY = "&q=\"" + URLEncoder.encode(qry,"UTF-8")+"\"";
        } catch (UnsupportedEncodingException e) {
            Log.e("QUERY_ERROR", "GoogleCustomSearchSender, query unsuportedEncoding");
        }
        QUERY_SORT_RANGE += dateFrom + ":" + dateTo;
    }


    private class GoogleSearchApiConnector extends AsyncTask<String, Void, String> {

        Context context;
        String url = new String();
        int counter;

        public GoogleSearchApiConnector(Context context, String url , int counter){
            this.context = context;
            this.url = url;
            this.counter = counter;
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                URL url = new URL(this.url + START_AT_COUNTER + counter);
                Log.e("COUNER",url.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

                //Remove comments if you need to output in JSON format
                String output = new String();
                for (String line; (line = br.readLine()) != null; output += line);
                conn.disconnect();
                return output;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
           if (result!=null){
               try {
                   ArrayList<GoogleSearchItem> arrivedItems = JsonObject.getArrayGoogleSearchItemsFromJSON(result);
                   items.addAll(arrivedItems);
                   counter += 10;
                   //Conter 100 is maximun in custom google search
                   if (arrivedItems.size() == 10 && counter<100){
                       new GoogleSearchApiConnector(context,url,counter).execute();
                   }else{
                       MainActivity mainActivity = (MainActivity) context;
                       mainActivity.addFoundedItems(items);
                   }
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

}
