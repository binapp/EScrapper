package com.binapp.escrapper.util;

import android.util.Log;

import com.binapp.escrapper.model.Article;
import com.binapp.escrapper.model.GoogleSearchItem;
import com.binapp.escrapper.model.Search;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by victor on 31/1/17.
 */

public class JsonObject {

    public static ArrayList<GoogleSearchItem> getArrayGoogleSearchItemsFromJSON(String json) throws JSONException {

        ArrayList<GoogleSearchItem> searchItems = new ArrayList<>();

        JSONObject jsonObject = new JSONObject(json);
        try{
            JSONArray jsonItems = jsonObject.getJSONArray("items");
            for (int i = 0 ; i < jsonItems.length() ; i++){
                JSONObject item = (JSONObject) jsonItems.get(i);
                String date = "";
                try {
                    JSONObject pagemap = item.getJSONObject("pagemap");
                    JSONArray metatags = pagemap.getJSONArray("metatags");
                    JSONObject metatag = (JSONObject) metatags.get(0);
                    date = metatag.getString("date");
                }catch(Exception e){
                    Log.e("NO_DATE","No date JSON");
                }

                String title = item.getString("title");
                String link = item.getString("link");
                String diary = item.getString("displayLink");


                searchItems.add(new GoogleSearchItem(title,link,diary,date));
            }
        }catch (Exception e){
            Log.e("NO_DATE","No items JSON");
        }

        return searchItems;
    }

    public static ArrayList<Article> getArticlesFromString(String articlesString){
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<Article>>(){}.getType();
        ArrayList<Article> articles = gson.fromJson(articlesString,listType);
        return articles;

    }

    public static String getStringFromArticlesArray(ArrayList<Article> articlesString){
        Gson gson = new Gson();
        return gson.toJson(articlesString);
    }

}
