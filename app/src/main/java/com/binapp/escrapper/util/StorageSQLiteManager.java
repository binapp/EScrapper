package com.binapp.escrapper.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.content.ContentValues.TAG;

/**
 * Created by victor on 24/2/17.
 */

class StorageSQLiteManager extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "EScrapper.db";


    public static final String SEARCH_TABLE_NAME = "searches_result";
    public static final String SEARCH_COLUMN_NAME_ID = "search_id";
    public static final String SEARCH_COLUMN_NAME_VALUE = "search_json";

    public static final String WORD_TABLE_NAME = "words";
    public static final String WORD_COLUMN_NAME_ID = "word_id";
    public static final String WORD_COLUMN_NAME_VALUE = "word_string";

    private static final String TEXT_TYPE = " TEXT";
    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS ";

    public StorageSQLiteManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String queryTable1 = "CREATE TABLE " + SEARCH_TABLE_NAME + " ("+SEARCH_COLUMN_NAME_ID+" INTEGER PRIMARY KEY AUTOINCREMENT," + SEARCH_COLUMN_NAME_VALUE + TEXT_TYPE+")";
        db.execSQL(queryTable1);
        String queryTable2 = "CREATE TABLE " + WORD_TABLE_NAME + " ("+WORD_COLUMN_NAME_ID+" INTEGER PRIMARY KEY AUTOINCREMENT," + WORD_COLUMN_NAME_VALUE + TEXT_TYPE+")";
        db.execSQL(queryTable2);

        Log.i(TAG, "TABLE CREATED");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String queryUpdate1 = SQL_DELETE_ENTRIES + SEARCH_TABLE_NAME;
        db.execSQL(queryUpdate1);
        String queryUpdate2 = SQL_DELETE_ENTRIES + WORD_TABLE_NAME;
        db.execSQL(queryUpdate2);

        onCreate(db);

        Log.i(TAG, "TABLE UPDATED");
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);

        Log.i(TAG, "TABLE DOWNGRADED");
    }
}
