package com.binapp.escrapper.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.binapp.escrapper.model.Search;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by victor on 24/2/17.
 */

public class StorageController {

    private Context context;

    public StorageController(Context context) {
        this.context = context;
    }

    public boolean addWordToDataBase(String word){

        //First delete to ensure not duplications
        deleteWordToDataBase(word);

        StorageSQLiteManager mDbHelper = new StorageSQLiteManager(context);
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(StorageSQLiteManager.WORD_COLUMN_NAME_VALUE, word);

        boolean isDone = db.insert(StorageSQLiteManager.WORD_TABLE_NAME, null, values) > 0;
        db.close();
        // Insert the new row, returning the primary key value of the new row
        return isDone;
    }

    public boolean deleteWordToDataBase(String word){
        StorageSQLiteManager mDbHelper = new StorageSQLiteManager(context);
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        String[] selectionArgs = {word};
        boolean isDone =  db.delete(StorageSQLiteManager.WORD_TABLE_NAME, StorageSQLiteManager.WORD_COLUMN_NAME_VALUE + " LIKE ?",selectionArgs) > 0;
        db.close();
        // Insert the new row, returning the primary key value of the new row
        return isDone;
    }

    public boolean addSeachToDataBase(Search search){

        Gson gson = new Gson();
        String json = gson.toJson(search);

        StorageSQLiteManager mDbHelper = new StorageSQLiteManager(context);
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(StorageSQLiteManager.SEARCH_COLUMN_NAME_VALUE, json);

        // Insert the new row, returning the primary key value of the new row
        boolean isDone =  db.insert(StorageSQLiteManager.SEARCH_TABLE_NAME, null, values) > 0;
        db.close();
        // Insert the new row, returning the primary key value of the new row
        return isDone;
    }

    public boolean deleteSearchToDataBase(Search search){

        Gson gson = new Gson();
        String json = gson.toJson(search);

        StorageSQLiteManager mDbHelper = new StorageSQLiteManager(context);
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String[] selectionArgs = {json };
        boolean isDone =  db.delete(StorageSQLiteManager.SEARCH_TABLE_NAME, StorageSQLiteManager.SEARCH_COLUMN_NAME_VALUE + " LIKE ?",selectionArgs) > 0;
        db.close();
        // Insert the new row, returning the primary key value of the new row
        return isDone;
    }

    public  ArrayList<Search> getSearchesFromDataBase(){
        StorageSQLiteManager mDbHelper = new StorageSQLiteManager(context);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        String[] projection = {StorageSQLiteManager.SEARCH_COLUMN_NAME_ID, StorageSQLiteManager.SEARCH_COLUMN_NAME_VALUE};
        String sortOrder = StorageSQLiteManager.SEARCH_COLUMN_NAME_ID + " DESC";

        Cursor cursor = db.query(
                StorageSQLiteManager.SEARCH_TABLE_NAME,     // The table to query
                projection,                                 // The columns to return
                null,                                       // The columns for the WHERE clause
                null,                                       // The values for the WHERE clause
                null,                                       // don't group the rows
                null,                                       // don't filter by row groups
                sortOrder                                   // The sort order
        );

        ArrayList<Search> searches = new ArrayList<>();
        if (cursor != null){

            if  (cursor.moveToFirst()) {
                do {
                    String firstName = cursor.getString(cursor.getColumnIndex(StorageSQLiteManager.SEARCH_COLUMN_NAME_VALUE));
                    Gson gson = new Gson();
                    Search search = gson.fromJson(firstName,Search.class);
                    searches.add(search);
                }while (cursor.moveToNext());
            }
            cursor.close();
        }

        db.close();
        return searches;
    }

    public  ArrayList<String> getWordsFromDataBase(){
        StorageSQLiteManager mDbHelper = new StorageSQLiteManager(context);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        String[] projection = {StorageSQLiteManager.WORD_COLUMN_NAME_ID, StorageSQLiteManager.WORD_COLUMN_NAME_VALUE};
        String sortOrder = StorageSQLiteManager.WORD_COLUMN_NAME_ID + " DESC";

        Cursor cursor = db.query(
                StorageSQLiteManager.WORD_TABLE_NAME,     // The table to query
                projection,                                 // The columns to return
                null,                                       // The columns for the WHERE clause
                null,                                       // The values for the WHERE clause
                null,                                       // don't group the rows
                null,                                       // don't filter by row groups
                sortOrder                                   // The sort order
        );

        ArrayList<String> words = new ArrayList<>();
        if (cursor != null){

            if  (cursor.moveToFirst()) {
                do {
                    String string = cursor.getString(cursor.getColumnIndex(StorageSQLiteManager.WORD_COLUMN_NAME_VALUE));
                    words.add(string);
                }while (cursor.moveToNext());
            }
            cursor.close();
        }

        db.close();
        return words;
    }
}
